package p5_MetodyStatyczne;

public class MetodyStatyczneIntro {
    public static void main(String[] args) {


        //    <sygnatura_metody> {
        //        //ciało metody                       <--- to co robi
        //    }


        // void nic nie zwraca

        int[][] tablicaDwuwymiarowa = new int[10][10];

        for (int poTablicach = 0; poTablicach < tablicaDwuwymiarowa.length; poTablicach++) {
            for (int wewnatrzTablic = 0; wewnatrzTablic < tablicaDwuwymiarowa[poTablicach].length; wewnatrzTablic++) {
                tablicaDwuwymiarowa[poTablicach][wewnatrzTablic] = (int) (Math.random() * 11);


            }


        }
        show(tablicaDwuwymiarowa);
    }

    public static void show(int[][] randomTable) {
        for (int[] tab : randomTable) {
            for (int value : tab) {
                System.out.print(value + " ");
            }
            System.out.println();
        }
    }
}
