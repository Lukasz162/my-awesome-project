package p10_inputOutput;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ReadHarry {
    public static void main(String[] args) {
        try(FileInputStream inputStream = new FileInputStream("C:\\Users\\lukas\\Desktop\\JavaDocuments\\HarryPotter.txt")){

            int i;
            while ((i = inputStream.read()) > -1) {
                System.out.print((char) i);
                Thread.sleep(5);
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
