package p10_inputOutput;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

public class Strumienie {
    public static void main(String[] args) throws IOException {
        InputStream input = System.in;
        System.out.println(input.read());

        PrintStream print = System.out;

        print.println("Hello");
    }
}
