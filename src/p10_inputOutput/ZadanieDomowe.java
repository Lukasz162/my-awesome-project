package p10_inputOutput;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ZadanieDomowe {
    public static void main(String[] args) {

        try (FileInputStream input = new FileInputStream("SaveAndRead.txt")) {

            int n;
            int number = 0;
            while ((n = input.read()) > -1) {
                n = (n % 48) % 10;
                number = number * 10 + n;
            }
            System.out.println(number);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
