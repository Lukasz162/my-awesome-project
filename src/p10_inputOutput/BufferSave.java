package p10_inputOutput;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BufferSave {
    public static void main(String[] args) {
        try (FileOutputStream outputStream = new FileOutputStream("sample.text")){
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);

            bufferedOutputStream.write(97);
            bufferedOutputStream.write(13);
            bufferedOutputStream.write(97);
            bufferedOutputStream.write(' ');
            bufferedOutputStream.write(97);
            bufferedOutputStream.write(97);
            bufferedOutputStream.write(97);
            bufferedOutputStream.write(97);
            bufferedOutputStream.write(97);

            bufferedOutputStream.flush();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
