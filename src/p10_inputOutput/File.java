package p10_inputOutput;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class File {
    public static void main(String[] args) {


        FileOutputStream outputSave = null;
        String s = "Ala ma kota" + "\n" + " :)";


        try(FileOutputStream out = new FileOutputStream("save.txt"))
        // *TryWithResources możemy stosować wtedy gdy nasz resource, implementuje interface [->[->[->[->[Closeable]<-]<-]<-]].


        {
            try {
                outputSave = new FileOutputStream("save.txt");

                outputSave.write(s.getBytes());

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                try {
                    outputSave.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

