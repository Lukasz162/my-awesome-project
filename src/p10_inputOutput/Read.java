package p10_inputOutput;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Read {
    public static void main(String[] args) {

        StringBuilder book = new StringBuilder();

        try (FileInputStream inputStream = new FileInputStream("C:\\Users\\lukas\\Desktop\\JavaDocuments\\HarryPotter.txt")) {

            int i;

            while ((i = inputStream.read()) > -1) {
                book.append((char)i);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(book);
    }
}
