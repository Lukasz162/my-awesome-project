package p10_inputOutput;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SaveAndRead {
    public static void main(String[] args) {

        int reading;
        try (FileInputStream inputStream = new FileInputStream("SaveAndRead.txt")) {
            reading = inputStream.read();




            while (true) {
                if (reading >= 0) {
                    System.out.println((char)reading);
                    inputStream.close();
                }
            }


        }
        catch (FileNotFoundException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}