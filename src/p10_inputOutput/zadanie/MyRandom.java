package p10_inputOutput.zadanie;

import javax.naming.Name;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class MyRandom {
    private String name;
    private String surname;

    public MyRandom(String name, String surname) {
        this.name = name;
        this.surname = surname;

    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "MyRandom{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MyRandom)) return false;
        MyRandom myRandom = (MyRandom) o;
        return Objects.equals(getName(), myRandom.getName()) &&
                Objects.equals(getSurname(), myRandom.getSurname());

    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname());
    }

    public static void main(String[] args) {
        double[] thingOne = new double[10];
        List<MyRandom> clients = new ArrayList<>();


        int x = 1;
        for (int i = 0; i <= thingOne.length - 1; i++) {
            thingOne[i] = (Math.random() * 11);

            System.out.println(x + ": " + (int) thingOne[i]);
            x++;
        }

        Arrays.stream(thingOne)
                .filter(array -> array > 5)
                .forEach(array -> {


                    System.out.print("More than five: ");
                    System.out.println((int) array);
                });
        System.out.println();

        clients.add(new MyRandom("Maciek", "Kowalski"));
        clients.add(new MyRandom("Maćko", "Bogdaniec"));
        clients.add(new MyRandom("Kamila", "Morawiec"));
        clients.add(new MyRandom("Amelia", "Niemyta"));
        clients.add(new MyRandom("Kamila", "Bogdaniec"));

        clients.stream()
                .filter(yolo -> yolo.getName().equals("Kamila"))
                .forEach(client ->
                        {
                            System.out.println("Name: " + client.getName());
                            System.out.println("Surname: " + client.getSurname() +"\n");
                        });

    }
}
