package p10_inputOutput.zadanie;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Point {
    private int x;
    private int y;

    public Point (int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        Point point = (Point) o;
        return getX() == point.getX() &&
                getY() == point.getY();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }
    public String csvConverter(){
        return this.x + "," + this.y + ",";

    }

    public static void main(String[] args) {
        List<Point> points = new ArrayList<>();
        List<Point> readedPoints = new ArrayList<>();
        points.add(new Point(7,4));
        points.add(new Point(1,2));
    try(FileOutputStream outputStream = new FileOutputStream("points.csv")) {

        for (Point point:points
             ) {
            outputStream.write(point.csvConverter().getBytes());

        }

        try (FileInputStream inputStream = new FileInputStream("points.csv")){
            int input;
            String inputToSplit = "";
            Point tmpPoint = new Point(0,0);
            while ((input = inputStream.read()) > -1){
                inputToSplit = inputToSplit + (char) input;
            }
            String[] tabOfXY = inputToSplit.split(",");
            for (int i = 0; i < tabOfXY.length - 1; i += 2) {
//                tmpPoint.setX(Integer.valueOf(tabOfXY[i]));
//                tmpPoint.setY(Integer.valueOf(tabOfXY[i+1]));
                readedPoints.add(new Point(Integer.valueOf(tabOfXY[i]),Integer.valueOf(tabOfXY[i+1])));
            }
            for (Point p:readedPoints) {
                System.out.println(p);

            }
        }







    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }




    }


}
