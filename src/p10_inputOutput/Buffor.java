package p10_inputOutput;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Buffor {
    public static void main(String[] args) {

        StringBuilder book = new StringBuilder();

        try (FileInputStream inputStream = new FileInputStream("C:\\Users\\lukas\\Desktop\\JavaDocuments\\HarryPotter.txt")){
            BufferedInputStream buffored = new BufferedInputStream(inputStream);

        int read;

        while ((read=buffored.read()) > -1) {
            book.append((char) read);
        }



        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(book);

    }
}
