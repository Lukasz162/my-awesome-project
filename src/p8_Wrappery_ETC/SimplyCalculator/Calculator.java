package p8_Wrappery_ETC.SimplyCalculator;

import java.util.Collections;
import java.util.Scanner;

public class Calculator {
    private String operationFromUser;
    private Scanner input;
    private String[] splitedOperationFromUser;
//    private int sumResult;


    public Calculator() {
        this.input = new Scanner(System.in);
    }

    public void load() {
        this.operationFromUser = input.nextLine();
    }

    public void splitStringBySpace() {
        splitedOperationFromUser = operationFromUser.split(" ");
    }

    public int operationBySign() {
        int result;
        for (int index = 1; index <= splitedOperationFromUser.length - 1; index ++) {

            if (splitedOperationFromUser[index].charAt(0) == '+') {
                result = Integer.valueOf(splitedOperationFromUser[0]) + Integer.valueOf(splitedOperationFromUser[index++]);
                return result;
            } else if (splitedOperationFromUser[index].charAt(0) == '-') {
                result = Integer.valueOf(splitedOperationFromUser[index - 1]) - Integer.valueOf(splitedOperationFromUser[index++]);
                return result;
            }
        }
        return 0;
    }


    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.load();
        calculator.splitStringBySpace();
        for (String word : calculator.splitedOperationFromUser) {
            System.out.println(word + " ");
        }
        System.out.println(calculator.operationBySign());
//        int dis;
//        String number = calculator.splitedOperationFromUser[1];
//        Integer result = Integer.valueOf(number);
//        System.out.println(result);
//        dis = result + 10;
//        System.out.println(dis);
//
//        String number2 = "61";
//        int result2 = Integer.parseInt(number2);
//        System.out.println(result2);
    }
}
