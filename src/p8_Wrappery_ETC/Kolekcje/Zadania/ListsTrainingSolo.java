package p8_Wrappery_ETC.Kolekcje.Zadania;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListsTrainingSolo {
    public static void main(String[] args) {
        List<String> crew = new ArrayList<>();
        crew.add("Maciek");
        crew.add("Jerzy");
        crew.add("Dawid");
        crew.add("Łukasz");
        crew.add("Staszek");

        Iterator<String> showCrew = crew.iterator();
        while (showCrew.hasNext()) {
//            System.out.println(showCrew.next());
            if (showCrew.next().equals("Jerzy")) {
                showCrew.remove();
            }
        }

        showCrew = crew.iterator();
        while (showCrew.hasNext()){
            System.out.println(showCrew.next());

        }


    }
}
