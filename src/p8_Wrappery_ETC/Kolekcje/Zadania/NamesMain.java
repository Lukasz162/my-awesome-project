package p8_Wrappery_ETC.Kolekcje.Zadania;

import java.util.*;

public class NamesMain {
    private String operationFromUser;
    private Scanner input;
    private LinkedList<String> clientNames = new LinkedList<>();

    public NamesMain() {
        this.input = new Scanner(System.in);
    }

    public void load() {
        this.operationFromUser = input.nextLine();
    }

    public void archiveNames() { this.clientNames.addFirst(operationFromUser); }

    public static void main(String[] args) {
        int limit = 0;
        NamesMain names = new NamesMain();
        do {
            names.load();
            System.out.println(names.operationFromUser);


            names.archiveNames();
            System.out.println(names.clientNames);
            limit++;

        } while (limit <= 10);


    }
}
