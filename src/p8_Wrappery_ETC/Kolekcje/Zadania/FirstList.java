package p8_Wrappery_ETC.Kolekcje.Zadania;

import java.util.ArrayList;
import java.util.List;

public class FirstList {
    public static void main(String[] args) {
        int i = 0;
        List<Integer> numbers = new ArrayList<>();

        while (i < 10) {
            numbers.add(i++);

//            System.out.print(numbers.size() + " ");
            System.out.println(++i);
        }
    }
}
