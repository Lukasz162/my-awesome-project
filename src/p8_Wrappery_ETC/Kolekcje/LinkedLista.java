package p8_Wrappery_ETC.Kolekcje;


import java.util.*;

public class LinkedLista {
    public static void main(String[] args) {

//        List<Integer> numbers = new ArrayList<>();
//        Set<String> words = new HashSet<>();
//
//        Map<String, Integer> sample = new HashMap<>();
//        sample.put("Key", 10);
//        System.out.println(sample.get("Key"));
//        ((HashMap<String, Integer>) sample).clone();
//        System.out.println(((HashMap<String, Integer>) sample).clone());
//        words.add("Hello set");


        List<Integer> numbersLinked = new LinkedList();
        List<Integer> numbersArray = new ArrayList<>();
        long start = System.currentTimeMillis();

        for (int i = 0; i < 1e7; i++) {
            numbersLinked.add(i);

        }
        long end = System.currentTimeMillis();
        System.out.println("LL: " + (end - start));
        start = System.currentTimeMillis();

        for (int i = 0; i < 1e7; i++) {
            numbersArray.add(i);
        }
        end = System.currentTimeMillis();
        System.out.println("AL: " + (end - start));


    }
}
