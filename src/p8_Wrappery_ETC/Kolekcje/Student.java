package p8_Wrappery_ETC.Kolekcje;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

public class Student {

    private String name;
    private String surname;




    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }


    public static void main(String[] args) {
        Map<Student, Double> grades = new HashMap<>();
        grades.put(new Student("Jacek", "Kowal"), 5d );
        grades.put(new Student("Jan", "Tam"), 2d);
        grades.put(new Student("Kamil", "Maski"), 2d);
        grades.put(new Student("Tomasz", "Kowalski"), 2d);

        Iterator<Map.Entry<Student,Double>> it = grades.entrySet().iterator();              // IMPORTANT HOW IT WORKS
        while (it.hasNext()){
            System.out.println(it.next());
        }

    }



    @Override
    public String toString () {
        return this.name + " " +this.surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name) &&
                Objects.equals(surname, student.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname);
    }
}
