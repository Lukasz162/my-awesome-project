package p8_Wrappery_ETC.Kolekcje;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Crew {


    public static void main(String[] args) {
        List<String> employees = new ArrayList<>();

        employees.add("Maria");
        employees.add("Rokita");
        employees.add("Jan");
        employees.add("Grzegorz");

        Iterator<String> hand = employees.iterator();
        while (hand.hasNext()) {
            System.out.println(hand.next());
            if (hand.next().equals("Maria")) {
                hand.remove();
            }
        }
        hand = employees.iterator();             // <------ Dlaczego tutaj znowu musimy napisać hand = employees[...]
        while (hand.hasNext()) {                // <------ I to??
            System.out.println(hand.next());    // Dlaczego wyżej nam to nie działa ?
        }

    }
}
