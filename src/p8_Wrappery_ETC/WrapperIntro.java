package p8_Wrappery_ETC;

import java.util.Scanner;

public class WrapperIntro {
    public static void main(String[] args) {
        System.out.println("Siemaneczko, mordeczko, wypijemy piweczeko?");
        Scanner input = new Scanner(System.in);
        String sentence = input.nextLine();
        String[] words = sentence.split(" ");
        for (String word : words) {
            System.out.println(word);

        }}

}
