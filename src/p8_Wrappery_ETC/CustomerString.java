package p8_Wrappery_ETC;


public class CustomerString {
    private String firstName;
    private String lastName;
    private String city;
    private int age;

    public String getCity() {
        return city;
    }

    public Integer getAge() {
        return age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

//    @Override
//    public String toString() {
//        return firstName + " " + lastName + " " + age + " " + city;
//    }


    public CustomerString(String firstName, String lastName, Integer age, String city) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.age = age;

    }
    public CustomerString(String personality){
        this (  personality.split(",") [0],
                personality.split(",") [1],
                 Integer.parseInt(personality.split(",") [2]),
                personality.split(",") [3]);

    }



    public static void main(String[] args) {
       String presonality = "Adam,Kowalski,24,Warszawa";
        CustomerString namesD = new CustomerString(presonality);
        System.out.println(presonality);
        String[] split = presonality.split("");
        System.out.println();
    }
}


