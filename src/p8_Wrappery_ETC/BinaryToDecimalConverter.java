package p8_Wrappery_ETC;

public class BinaryToDecimalConverter {
    private String binary;

    public static int convert(String binary) {

        int sum = 0;

        for (int index = 0; index < binary.length(); index++) {
            System.out.println(binary.charAt(binary.length() - 1 - index) + ", " + index);
            if (binary.charAt(binary.length() - 1 - index) == '1') {
                sum += Math.pow(2, index);
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(convert("11101"));
    }


}