package p8_Wrappery_ETC;

public class PolindromeChecker {

    public static boolean isPolindrome(String word) {

        for (int index = 0; index < word.length() / 2; index++) {
            if (word.charAt(index) != word.charAt(word.length() - index - 1)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(PolindromeChecker.isPolindrome("kajak"));
    }
}
