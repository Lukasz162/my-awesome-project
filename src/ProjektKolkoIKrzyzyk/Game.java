package ProjektKolkoIKrzyzyk;

import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        System.out.println("HELLO!");
        System.out.println();
        System.out.println("Podajcie wymiar planszy!");
        int size = new Scanner(System.in).nextInt();
        char[][] board = new char[size][size];

        showBoard(board);

        int player = 0;
        double count = Math.pow(board.length, 2);
        boolean nobodyWin = true;
        while (count > 0 && nobodyWin) {
            if (player % 2 == 0) {
                readAndCheckAndMark(board, 'X');
                nobodyWin = !isWin(board, 'X');
            } else {
                readAndCheckAndMark(board, 'O');
                nobodyWin = !isWin(board, 'O');
            }
            showBoard(board);

            player++;
            count--;
        }
        if (nobodyWin) {
            System.out.println("Draw");
        } else {
            if (player % 2 == 0) {
                System.out.println();
                System.out.println("O ");
            } else {
                System.out.println();
                System.out.println("X ");
            }
            System.out.println("Win!");
        }


    }

    public static void showBoard(char[][] board) {

        for (char i[] : board) {
            for (char j : i) {

                if (j > 0) {
                    System.out.print("|" + j);
                } else {
                    System.out.print("|_");
                }

            }
            System.out.print("|");
            System.out.println();
        }
    }

    public static void mark(char[][] board, int row, int column, char sign) {
        board[row][column] = sign;
    }


    public static void readAndCheckAndMark(char[][] board, char sign) {

        int row;
        int column;
        do {


            System.out.println("Player " + sign);
            System.out.println("wybierz wiersz");
            row = new Scanner(System.in).nextInt();
            System.out.println("wybierz kolumne");
            column = new Scanner(System.in).nextInt();


        } while (isMarked(board, row, column));
        mark(board, row, column, sign);
    }

    public static boolean isMarked(char[][] board, int row, int column) {
        if (!(row < board.length && row >= 0 && column < board.length && column >= 0) || board[row][column] != 0) {
            System.out.println("Błędny wybór!");
            return true;
        }

        return false;
    }

    public static boolean isWin(char[][] board, char sign) {
        int counterInRightDiagonal = 0;
        int counterInLeftDiagonal = 0;
        for (int row = 0; row < board.length; row++) {
            int counterInRow = 0;
            int counterInColumn = 0;
            for (int column = 0; column < board.length; column++) {
                if (board[row][column] == sign) {
                    counterInRow++;
                }
                if (board[column][row] == sign) {
                    counterInColumn++;
                }
                if (row == column && board[row][column] == sign) {
                    counterInRightDiagonal++;
                }
                if (board.length - 1 == row + column && board[row][column] == sign) {
                    counterInLeftDiagonal++;
                }
            }
            if (counterInRow == board.length || counterInColumn == board.length) {
                return true;
            }
        }
        if (counterInRightDiagonal == board.length || counterInLeftDiagonal == board.length) {
            return true;
        }
        return false;
    }

}