package ProjektKolkoIKrzyzyk.TicTacToeObjects;


import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        System.out.println("HELLO!");
        System.out.println();
        System.out.println("Podajcie wymiar planszy!");
        Board board = new Board();
        LogicMethods readAndCheckAndMark = new LogicMethods();
readAndCheckAndMark.readAndCheckAndMark( board.board,'X');

        board.showBoard();

        int player = 0;
        double count = Math.pow(board.board.length, 2);
        boolean nobodyWin = true;
        while (count > 0 && nobodyWin) {
            if (player % 2 == 0) {
                readAndCheckAndMark.readAndCheckAndMark(board.board, 'X');
                nobodyWin = !isWin(board.board, 'X');
            } else {
                readAndCheckAndMark.readAndCheckAndMark(board.board, 'O');
                nobodyWin = !isWin(board.board, 'O');
            }
            board.showBoard();

            player++;
            count--;
        }
        if (nobodyWin) {
            System.out.println("Draw");
        } else {
            if (player % 2 == 0) {
                System.out.println();
                System.out.println("O ");
            } else {
                System.out.println();
                System.out.println("X ");
            }
            System.out.println("Win!");
        }


    }

    public static boolean isWin(char[][] board, char sign) {
        int counterInRightDiagonal = 0;
        int counterInLeftDiagonal = 0;
        for (int row = 0; row < board.length; row++) {
            int counterInRow = 0;
            int counterInColumn = 0;
            for (int column = 0; column < board.length; column++) {
                if (board[row][column] == sign) {
                    counterInRow++;
                }
                if (board[column][row] == sign) {
                    counterInColumn++;
                }
                if (row == column && board[row][column] == sign) {
                    counterInRightDiagonal++;
                }
                if (board.length - 1 == row + column && board[row][column] == sign) {
                    counterInLeftDiagonal++;
                }
            }
            if (counterInRow == board.length || counterInColumn == board.length) {
                return true;
            }
        }
        if (counterInRightDiagonal == board.length || counterInLeftDiagonal == board.length) {
            return true;
        }
        return false;
    }

}





