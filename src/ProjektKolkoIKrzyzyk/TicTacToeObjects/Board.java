package ProjektKolkoIKrzyzyk.TicTacToeObjects;

import java.util.Scanner;

public class Board {
    char[][]board;
    public void showBoard() {

        for (char i[] : board) {
            for (char j : i) {

                if (j > 0) {
                    System.out.print("|" + j);
                } else {
                    System.out.print("|_");
                }

            }
            System.out.print("|");
            System.out.println();
        }
    }

    public Board() {
        int size = new Scanner(System.in).nextInt();
        board = new char[size][size];

    }
}