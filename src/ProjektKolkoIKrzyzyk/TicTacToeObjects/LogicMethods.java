package ProjektKolkoIKrzyzyk.TicTacToeObjects;

import java.util.Scanner;

public class LogicMethods {

    public void mark(char[][] board, int row, int column, char sign) {
        board[row][column] = sign;
    }

    public void readAndCheckAndMark(char[][] board, char sign) {
        LogicMethods mark = new LogicMethods();
        int row;
        int column;
        do {


            System.out.println("Player " + sign);
            System.out.println("wybierz wiersz");
            row = new Scanner(System.in).nextInt();
            System.out.println("wybierz kolumne");
            column = new Scanner(System.in).nextInt();


        } while (isMarked(board, row, column));
        mark.mark(board, row, column, sign);
    }
    public boolean isMarked(char[][] board, int row, int column) {
        if (!(row < board.length && row >= 0 && column < board.length && column >= 0) || board[row][column] != 0) {
            System.out.println("Błędny wybór!");
            return true;
        }

        return false;
    }
}
