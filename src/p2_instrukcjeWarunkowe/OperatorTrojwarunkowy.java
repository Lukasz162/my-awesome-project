package p2_instrukcjeWarunkowe;

import java.util.Scanner;

public class OperatorTrojwarunkowy {
    public static void main(String[] args) {
        System.out.println("Podaj x\n");
        int x = new Scanner(System.in).nextInt();
        int abs = (x>0) ? x : -x;
        System.out.println(abs);
    }
}
