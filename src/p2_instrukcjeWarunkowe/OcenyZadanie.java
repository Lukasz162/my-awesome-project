package p2_instrukcjeWarunkowe;

import java.util.Scanner;

public class OcenyZadanie {
    public static void main(String[] args) {
        System.out.println("Maksymalna ilość pkt");
        int max = new Scanner(System.in).nextInt();
        System.out.println("Ilość punktów otrzymanych przez ucznia:");
        int pkt = new Scanner(System.in).nextInt();
        double procent = (double) pkt / max;
        System.out.println((int) (procent * 100) + "%");
        int ocena = -1;



        if (procent <= 0.5) {
            ocena = 2;
        }
        if (procent > 0.5 && procent <= 0.7) {
            ocena = 3;
        }
        if (procent > 0.7 && procent <= 0.9) {
            ocena = 4;
        }
        if (procent > 0.9) {
            ocena = 5;
        }

        System.out.println("Uczeń dostał ocenę " + ocena);
    }
}
