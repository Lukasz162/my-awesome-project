package p6_objects;

import java.util.Scanner;

public class Time {

    private int hours;
    private int minutes;
    public final int MAX_HOURS = 24;
    public final int MAX_MINUTES = 60;


    public Time() {

    }

    public Time(int hours, int minutes) {

        this.hours = hours;
        this.minutes = minutes;
    }

    @Override
    public String toString() {
        return "Hours: " + this.hours + ":" + this.minutes;
    }

    public void printHours() {

        if (hours < 10) {

            System.out.print("0" + hours);
            System.out.print(":");
        } else {
            System.out.print(hours);
            System.out.print(":");
        }
        if (minutes < 10) {
            System.out.print("0" + minutes);
        } else {
            System.out.println(minutes);
        }
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;

    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getMinutes() {
        return minutes;
    }

    public void addHours(int hours) {
        this.hours = (this.hours += hours) % MAX_HOURS;

    }

    public void addMinutes(int minutes) {
        addHours((this.minutes += minutes) / 60);
        this.minutes = this.minutes %= MAX_MINUTES;

    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Time)) {
            return false;
        }
        Time time = (Time) object;
        if (this == time) {
            return true;
        }
        if (this.hours != ((Time) object).hours) {
            return false;
        } else
            return this.minutes == ((Time) object).minutes;
    }

    public Time diff(Time time) {
       Time result = new Time(0,0);

       int diferenceInMinutes = Math.abs((this.hours * 60 + this.minutes) - (time.hours * 60 + time.minutes));
        result.addMinutes(diferenceInMinutes);
       return result;
    }

    public static void main(String[] args) {

        Time time = new Time(0, 10);
        Time time2 = new Time(23, 57);
        System.out.println(time2.equals(time));
        System.out.println(time.diff(time2));
    }
}



