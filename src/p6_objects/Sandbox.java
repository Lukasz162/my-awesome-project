package p6_objects;

public class Sandbox {
    public static void main(String[] args) {
        Car audi = new Car();
        audi.wheelsNumber = 4;
        audi.gasType = 'd';

        audi.showAlert();
    }
}
