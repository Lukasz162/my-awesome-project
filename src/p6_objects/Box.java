package p6_objects;

public class Box {
    int x;
    int y;
    int z;

    public Box(){
        this(10);
        System.out.println("Bezparametrowy");
    }

    public Box(int x) {
        this(x,10);
        System.out.println("Jednoparametrowy");
    }

    public Box(int x, int y) {
        this(x,y,10);
        System.out.println("Dwuparametrowy");
    }
    public Box(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
        System.out.println("Trzyparametrowy");
    }

    public int volume(){
        return x*y*z;
    }


}
