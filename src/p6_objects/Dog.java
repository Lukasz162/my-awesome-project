package p6_objects;

public class Dog {
    int age;
    int newAge;

    void getNewAge() {
        System.out.println("Now dog have " + newAge);
    }

    void showAge() {
        System.out.println("Dog have " + age + " years");
    }

    void setSzczekaj() {
        System.out.println("Szczek, szczek");
    }

    void setWarcz() {
        System.out.println("Wark, wark");
    }
}
