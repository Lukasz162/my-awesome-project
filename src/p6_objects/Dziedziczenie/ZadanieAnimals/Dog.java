package p6_objects.Dziedziczenie.ZadanieAnimals;

public class Dog extends Animal{
    @Override
    public String toString() {
        return "Dog " + age;
    }
    @Override
    public void speak(){
        System.out.println("Dog speak");
    }
}
