package p6_objects.Dziedziczenie.ZadanieAnimals;

public class Cat extends Animal{
    @Override
    public String toString() {
        return "Cat " + age;
    }
    @Override
    public void speak(){
        System.out.println("Cat speak");
    }
}
