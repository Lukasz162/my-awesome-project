package p6_objects.Dziedziczenie.ZadanieAnimals;

public class Bird extends Animal {
    @Override
    public String toString() {
        return "Bird " + age;
    }
    @Override
    public void speak(){
        System.out.println("Bird speak");
    }
}
