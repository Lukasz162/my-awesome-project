package p6_objects.Dziedziczenie.ZadaniePoint;

public class Point {
    private int x;
    private int y;

//    public Point() {
//    }

    public Point(int x, int y) {

        this.x = x;
        this.y = y;


    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;

    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "X:" + this.x + " Y:" + this.y;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Point)) {
            return false;                           //overenginering - NIE PISAC BEZ "wąsów", "{}"
        }
        Point point = (Point) obj;
        if (this == point) {
            return true;
        }
        return this.x == point.x && point.y == this.y;
    }

    public double vectorLenght() {

        return distance(new Point(0,0));

    }

    double distance(Point point) {
        Point point1 = new Point(x, y);
        return Math.sqrt(Math.pow(this.x - point.x, 2) + Math.pow(this.y - point.y, 2));
    }

}