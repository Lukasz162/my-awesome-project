package p6_objects.Dziedziczenie.ZadaniePoint;


public class Rectangle {
    private Point downLeft;
    private Point upperRight;
    private Point downRight;
    private Point upperLeft;
    private int smartCounter;

    private Rectangle() {
    }

//    private Rectangle(Point downLeft, Point uppperRight, Point downRight, Point upperLeft) {
//        this.downLeft = downLeft;
//        this.upperRight = uppperRight;
//        this.downRight = downRight;
//        this.upperLeft = upperLeft;
//    }


    private Rectangle(int x1, int y1, int x2, int y2) {
        this.upperLeft = new Point(x1, y2);
        this.downLeft = new Point(x1, y1);
        this.upperRight = new Point(x2, y2);
        this.downRight = new Point(x2, y1);


    }

    @Override
    public String toString() {
        return "DownLeft " + downLeft + " UpperRight " + upperRight;
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Rectangle)) {
            return false;
        }
        Rectangle rectangle = (Rectangle) obj;
        if (this == rectangle) {
            return true;
        }
        return rectangle.downLeft.equals(this.downLeft) && rectangle.upperRight.equals(this.upperRight);
    }

    private int delta(char sign) {
        switch (sign) {
            case 'y':
                return this.upperRight.getY() - this.downLeft.getY();
            case 'x':
                return this.upperRight.getX() - this.downLeft.getX();
        }
        return -1;
    }

    public double perimeter() {
        return 2 * (delta('y')) + 2 * (delta('x'));
    }

    public double area() {
        return (delta('x')) * (delta('y'));
    }

    private void isRectangleInside(int isInsideRectangle) {

        if (isInsideRectangle == 0) {
            System.out.println("Recentagle is full Outside");

        } else if (isInsideRectangle == 4) {
            System.out.println("Recentagle is full Inside");

        }

    }


    public boolean isPointInside(Rectangle recentagleTwo) {
        Rectangle r1 = this;             //r1 - rectangleOne, //r2 - rectangleTwo
        Rectangle r2 = recentagleTwo;


        int isInsideRectangle = 0;

        if (r1.downLeft.getX() <=                         // czy X prawego górnego wierzchołka znajduje się wewnątrz R1
                r2.upperRight.getX() && r2.upperRight.getX()
                <= r1.upperRight.getX() &&

                r1.downRight.getY() <=                    // czy Y prawego górnego wierzchołka znajduje się wewnątrz R1
                        r2.upperRight.getY() && r2.upperRight.getY()
                <= r1.upperRight.getY()) {
            System.out.println("upRight is inside R1");
            isInsideRectangle++;
        }
        if (r1.downLeft.getX() <=                       // czy X lewego górnego wierzchołka znajduje się wewnątrz R1
                r2.upperLeft.getX() && r2.upperLeft.getX()
                <= r1.upperRight.getX() &&

                r1.downRight.getY() <=                  // czy Y lewego górnego wierzchołka znajduje się wewnątrz R1
                        r2.upperLeft.getY() && r2.upperLeft.getY()
                <= r1.upperRight.getY()) {
            System.out.println("upLeft is inside R1");
            isInsideRectangle++;
        }
        if (r1.downLeft.getX() <=                       // czy X prawego dolnego wierzchołka znajduje się wewnątrz R1
                r2.downRight.getX() && r2.downRight.getX()
                <= r1.upperRight.getX() &&

                r1.downRight.getY() <=
                        r2.downRight.getY() && r2.downRight.getY()
                <= r1.upperRight.getY()) {              // czy Y prawego dolnego wierzchołka znajduje się wewnątrz R1
            System.out.println("downRight is inside R1");
            isInsideRectangle++;

        }
        if (r1.downLeft.getX() <=                       // czy X lewego dolnego wierzchołka znajduje się wewnątrz R1
                r2.downLeft.getX() && r2.downLeft.getX()
                < r1.upperRight.getX() &&

                r1.downRight.getY() <=                  // czy Y lewego dolnego wierzchołka znajduje się wewnątrz R1
                        r2.downLeft.getY() && r2.downLeft.getY()
                <= r1.upperRight.getY()) {
            System.out.println("downLeft is inside R1");
            isInsideRectangle++;

            isRectangleInside(isInsideRectangle);
            return true;
        } else {
            isRectangleInside(isInsideRectangle);


            return false;
        }
    }

    public boolean isOutsideButStillGood(Rectangle recentagleTwoToCheck) {
        Rectangle r1 = this;
        Rectangle r2Check = recentagleTwoToCheck;


        return r1.downLeft.getX() <=
                r2Check.downLeft.getX() || r1.downLeft.getX() >=
                r2Check.downLeft.getX() && r2Check.downLeft.getX()
                <= r1.downRight.getX() &&

                r1.downLeft.getX() <=
                        r2Check.downRight.getX() && r2Check.downRight.getX()
                >= r1.downRight.getX() || r2Check.downRight.getX()
                <= r1.downRight.getX() &&

                r2Check.downRight.getY() <=
                        r1.downRight.getY() && r1.upperRight.getY()
                <= r2Check.upperRight.getY();

    }

    public static void main(String[] args) {
        Rectangle rectangleOne = new Rectangle(4, 4, 8, 8);
        Rectangle recentagleTwo = new Rectangle(5  , 3, 7, 10);
        Rectangle smartCounter = new Rectangle();
        Rectangle isRectangle = new Rectangle();
        smartCounter.smartCounter = 0;

        if (rectangleOne.isPointInside(recentagleTwo)) {
            smartCounter.smartCounter++;
        }
        if (smartCounter.smartCounter == 0) {
            recentagleTwo.isPointInside(rectangleOne);
        }
        if (rectangleOne.isOutsideButStillGood(recentagleTwo)) {

            System.out.println("FULL OUTSIDE BUT STILL INTERSECT");
        }
    }
}