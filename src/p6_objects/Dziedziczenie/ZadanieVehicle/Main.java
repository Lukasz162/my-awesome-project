package p6_objects.Dziedziczenie.ZadanieVehicle;

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        Bike bike = new Bike();
        Car car2 = new Car();
        car.manufactureYear = 2000;
        bike.manufactureYear = 2018;
        car2.manufactureYear = 2000;
        System.out.println(car);

        System.out.println(car.equals(car2));

    }
}
