package p6_objects.Dziedziczenie.ZadanieVehicle;

import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;

public class Car extends Vehicle {
    @Override
    public String toString() {
        return "Car " + super.toString();                   // super (takie doklejenie) wyswietl Car + stringa
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Car)) {
            return false;                                                    // instanceof - czy mój obiekt jest klasy ... ?
        }
        Car car = (Car) obj;
        if (this == car) {
            return true;
        }
        return this.manufactureYear == car.manufactureYear;


    }

}

