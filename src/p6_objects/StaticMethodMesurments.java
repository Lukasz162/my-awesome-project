package p6_objects;

import java.util.Scanner;

public class StaticMethodMesurments {
    public static void main(String[] args) {
        Point pointOne;
        pointOne = new Point();

        Point pointTwo;
        pointTwo = new Point();

        System.out.println("xOne: ");
        pointOne.xPoint = new Scanner(System.in).nextInt();
        System.out.println("yOne: ");
        pointOne.yPoint = new Scanner(System.in).nextInt();
        System.out.println("xTwo: ");
        pointTwo.xPoint = new Scanner(System.in).nextInt();
        System.out.println("yTwo: ");
        pointTwo.yPoint = new Scanner(System.in).nextInt();

        System.out.println(distanceBetweenPoints(pointOne.xPoint,pointOne.yPoint,pointTwo.xPoint,pointTwo.yPoint));



    }
    public static double distanceBetweenPoints (int xOne, int yOne, int xTwo, int yTwo) {

        double distance;
        distance = Math.sqrt(Math.pow(xOne - xTwo, 2) + Math.pow(yOne - yTwo, 2));
        return distance;
    }

}
