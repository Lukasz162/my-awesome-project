package p6_objects;

import java.util.Scanner;

public class TimeMain {
    public static void main(String[] args) {
        while (true) {
        int hours = new Scanner(System.in).nextInt();
        int minutes = new Scanner(System.in).nextInt();

        Time time = new Time(hours, minutes);

           time.printHours();
           time.setHours(hours);
           time.setMinutes(minutes);

           System.out.println();
           time.printHours();
           time.addHours(20);
           System.out.println();
           time.printHours();

       }
    }
}
