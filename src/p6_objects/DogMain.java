package p6_objects;

public class DogMain {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.age = 4;
        dog.newAge = 6;

        dog.showAge();
        dog.getNewAge();
        dog.setSzczekaj();
        dog.setWarcz();
    }
}
