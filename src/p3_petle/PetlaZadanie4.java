package p3_petle;

import java.util.Scanner;

public class PetlaZadanie4 {
    public static void main(String[] args) {
        System.out.println("Wpisz limit");
        int number1 = 1;
        int number2 = new Scanner(System.in).nextInt();
        int sum = 1;


        while (number1 <= number2) {

            sum = sum * number1;
            System.out.println(sum);
            number1 = number1 + 1;  // 1
        }
    }
}
