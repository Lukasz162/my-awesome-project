package p3_petle;

import java.util.Scanner;

public class PetlaForZadanie6 {
    public static void main(String[] args) {


        int z = 5;
        for (int x = 1; x <= z; x++) {

            for (int y = 1; y <= z; y++) { // x =1 y =1 spacja =1, \X_

                System.out.print("X");
                for (int spacja = 1; spacja <= z; spacja++) {
                    System.out.print(" ");
                    if (spacja == z && y == z) {
                        System.out.print("X");
                    }

                }
            }
            System.out.println();
        }
    }
}
