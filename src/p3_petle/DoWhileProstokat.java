package p3_petle;

import java.util.Scanner;

public class DoWhileProstokat {
    public static void main(String[] args) {

        int liczbaGwiazdekWRzedzie = new Scanner(System.in).nextInt();
        int liczbaGwiazdekKolumna = new Scanner(System.in).nextInt();

        while (liczbaGwiazdekWRzedzie > 0) {
            System.out.print("X");
            liczbaGwiazdekWRzedzie--;
            System.out.println(" ");
        }
        while (liczbaGwiazdekKolumna > 0) {
            liczbaGwiazdekKolumna--;
            System.out.println("Y");
        }
    }
}
