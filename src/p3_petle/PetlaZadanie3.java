package p3_petle;

import java.util.Scanner;

public class PetlaZadanie3 {
    public static void main(String[] args) {
        System.out.println("Podaj dwie liczby całkowite");
        int pierwszaLiczba = new Scanner(System.in).nextInt();
        int secondNumber = new Scanner(System.in).nextInt();
        if (pierwszaLiczba > secondNumber) {
            while (pierwszaLiczba >= secondNumber) {
                System.out.println(pierwszaLiczba);
                pierwszaLiczba = pierwszaLiczba - 1;
            }
        } else {
            while (pierwszaLiczba <= secondNumber) {
                System.out.println(pierwszaLiczba);
                pierwszaLiczba = pierwszaLiczba + 1;
            }
        }
    }
}
