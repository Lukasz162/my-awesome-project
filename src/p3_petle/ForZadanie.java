package p3_petle;

import java.util.Scanner;

public class ForZadanie {
    public static void main(String[] args) {
        int treeHeight = new Scanner(System.in).nextInt();



        for (int y = 1; y <= treeHeight; y++) {
            for (int x = 1; x <= treeHeight-y; x++) {
                System.out.print(" ");
            }

            for (int x = 1; x <= 2 * y - 1; x++) {
                System.out.print("x");
            }
            System.out.println();
        }

    }
}

