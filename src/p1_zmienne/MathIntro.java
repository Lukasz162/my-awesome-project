package p1_zmienne;

import java.util.Scanner;

public class MathIntro {
    public static void main(String[] args) {
        System.out.println("Podaj promień koła: ");
        double promienKola = new Scanner(System.in).nextDouble();
        double poleKola = Math.PI * Math.pow(promienKola, 2);
        System.out.println(poleKola);

        }
}
