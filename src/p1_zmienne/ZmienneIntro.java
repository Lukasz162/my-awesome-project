package p1_zmienne;

public class ZmienneIntro {
    public static void main(String[] args) { // psvm (public static void main)
        int x = 10; // deklraracja (int x) plus inicjalizacja (=10)
        int y = 20;
        int mojaZmienna = 50;
        System.out.println ("Wartość X to "+x);   // sout + TAB (skrót do wydruku)
        x = 20;
        System.out.println ("Wartość X to "+x);
        x = mojaZmienna;
        System.out.println("Wartość X to "+x);
        int z; // deklaracja zmiennej ale bez przypisywania wartosci (bez inicjalizacji)
        //System.out.println("Z = "+z); // to nie zadziala, nie mozna uzywac zmiennej zanim jej nie zainicjalizujemy
        z = 100;
        System.out.println("Wartość Z to "+z);
//        int 1x = 50; // nieWolnoZaczynaćOdCyfrNazwZmiennych    ===PISZEMY TYLKO TAK===
//    int ZMIENNA = 5;
//    int zmienna# = 5;
//    int int;
    int pierwszaZmienna, drugaZmienna;
    pierwszaZmienna = 10;
    int trzeciaZmienna = 1, czwartaZmienna = 2;


    }
}
