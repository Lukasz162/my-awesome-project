package p1_zmienne;

import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        System.out.println("Podaj ile masz kasy:");
        double  kasa = new Scanner(System.in).nextDouble();
        System.out.println("Ile kosztuje laptop?");
        double laptop = new Scanner(System.in).nextDouble();
        int iloscLaptopow = (int)(kasa/laptop);   // (INT) - rzutowanie wartości DOUBLE na wartość INT
        double reszta = kasa%laptop;
        if (iloscLaptopow >0){
        System.out.println("Kupisz " +iloscLaptopow +" laptopów");
        System.out.println("Zostanie Ci:" +reszta +" zł");}
        if (iloscLaptopow <= 0){
            System.out.println("Nie kupisz żadnego laptopa, jedynie gumy kulki");
        }
    }
}
