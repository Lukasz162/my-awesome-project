package p1_zmienne;

import java.util.Scanner;

public class WlasyProgram {
    public static void main(String[] args) {
        int c = 0;
        do {

            {
                System.out.println("\nWitaj użytkowniku, chciałbyś porozmawiać ze mną?\n");
                System.out.println("Jeżeli tak to wybierz 1 \n\nJeżeli nie to wybierz 2\n");
                int a = new Scanner(System.in).nextInt();
                c++;
                switch (a) {
                    case 1:
                        System.out.println("Super, więc powiedz mi czy lubisz Łukasza?");
                        System.out.println("1 - TAK\n2 - NIE\n");
                        int b = new Scanner(System.in).nextInt();
                        if (b == 1) {
                            System.out.println("Łukasz jest z tego powodu bardzo zadowolony");
                            c=c+2;
                        } else if (b == 2) {
                            System.out.println("Łukasz też Cię nie lubi");
                        } else if (b >= 0 || b <= 3) {
                            System.out.println("Zły wybór, zapraszam ponownie");
                        }

                        break;
                    case 2:
                        System.out.println("Rozumiem, to do zobaczenia następnym razem");
                        break;
                    default:
                        System.out.println("Błędny wybór, zapraszamy ponownie");
                }
            }
        }while(c <= 2);


    }
}
