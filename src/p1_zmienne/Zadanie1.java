package p1_zmienne;

public class Zadanie1 {
    public static void main(String[] args) {
        int zmiennaA = 10, zmiennaB = 20, zmiennaC = 30;
        System.out.println(zmiennaA);
        System.out.println(zmiennaB);
        System.out.println(zmiennaC);
        System.out.println("---Zmieniam wartości---");
        zmiennaA = 200;
        zmiennaB = 300;
        zmiennaC = 400;
        System.out.println(zmiennaA);
        System.out.println(zmiennaB);
        System.out.println(zmiennaC);
        byte byteA = 100;               // byte short int long - typy zmienne
        System.out.println(byteA);
//        typy zmiennych zmiennoprzecinkowych (float, double)
        float zmiennaFloat = 1.33f; // 1.33f - float (d - double)
        double zmiennaDouble = 1.33d;


        int iloraz2 = 10 / 3;
        System.out.println(iloraz2);
        double ilorazDouble = 10.0 / 3;  //jezeli nie ma kropek to Java widzi jako INT
        System.out.println(ilorazDouble);
    }
}
