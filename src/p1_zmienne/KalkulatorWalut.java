package p1_zmienne;

import java.util.Scanner;

public class KalkulatorWalut {
    public static void main(String[] args) {
        System.out.println("Podaj kwotę w zł: ");

        double kwotaWPLN = new Scanner(System.in).nextDouble();

        System.out.println("Podana kwota to: " + kwotaWPLN + "zł");
        System.out.println("Podaj kurs euro: ");

        double kursEuro = new Scanner(System.in).nextDouble();
        double wynik = kwotaWPLN / kursEuro;

        System.out.println(kwotaWPLN + "zł to " + wynik + "€");
    }
}
