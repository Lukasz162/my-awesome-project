package p1_zmienne;

public class Inkrementacja {
    public static void main(String[] args) {
        // klasycznie
        int x = 10;
        x= x+1;  // +1 (10+1=11) INKREMENTACJA
        x++;
        System.out.println(x); // 12
        x--; // DEKREMENTACJA
        System.out.println(x);// 11
        System.out.println(x++);// POST"INKREMENTACJA (x++ [po x])
        System.out.println(x);//12
        System.out.println(++x);  //PRE"INKREMENTACJA (--x [przed x])
    }
}
