package p4_tablice;

import java.util.Scanner;

public class TabliceCwiczenia {
    public static void main(String[] args) {
        int[] tablicaWyjsciowa = {1, 3, 4, 6, 7, 8, 4, 3, 2, 2, 4, 5, 6};
        int dlugosc = tablicaWyjsciowa.length;
        int[] nowaTablica = new int[dlugosc];


        int index = dlugosc - 1;
        for (int i = 0; i < dlugosc; i++) {

            nowaTablica[i] = tablicaWyjsciowa[dlugosc - 1 - i];
        }
        int i = 0;
        while (i < dlugosc) {
            System.out.print(nowaTablica[i] + ", ");
            i++;
        }
        System.out.println();
        int z = 0;
        do {
            System.out.print(nowaTablica[z] + ", ");
            z++;
        } while (z < dlugosc);
    }
}
