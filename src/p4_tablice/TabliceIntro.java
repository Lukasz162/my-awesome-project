package p4_tablice;

public class TabliceIntro {
    public static void main(String[] args) {
        int [] mojaPierwszaTablica = new int [10]; // <--- typ oraz rozmiar tablicy
        int mojaDrugaTablica[] = new int [10];  // <--- tak też może być

        mojaPierwszaTablica[1]=3;
        mojaPierwszaTablica[0]=123;
        System.out.println(mojaPierwszaTablica[1]);

        int[] mojaTrzeciaTablica = {10,13};   //<--- Tablica o wielkości 2 ( [0]=10 [1]=13 )
        System.out.println(mojaTrzeciaTablica[1]);
    int dlugoscTablicy =mojaPierwszaTablica.length;
        System.out.println(dlugoscTablicy);
    }
}
