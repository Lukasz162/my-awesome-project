package p4_tablice.p4_1_tabliceWielowymiarowe;

public class TabliceWielowymiaroweRandomDwa {
    public static void main(String[] args) {

        int tablicaWielowymiarowa[][] = new int[4][];


        int[] piewszaTablica = new int[4];
        piewszaTablica[0] = (int) (Math.random() * 11);
        piewszaTablica[1] = (int) (Math.random() * 11);
        piewszaTablica[2] = (int) (Math.random() * 11);
        piewszaTablica[3] = (int) (Math.random() * 11);

        int drugaTablica[] = new int[4];
        drugaTablica[0] = (int) (Math.random() * 11);
        drugaTablica[1] = (int) (Math.random() * 11);
        drugaTablica[2] = (int) (Math.random() * 11);
        drugaTablica[3] = (int) (Math.random() * 11);

        tablicaWielowymiarowa[0] = piewszaTablica;
        tablicaWielowymiarowa[1] = drugaTablica;

        int najmniejsza = tablicaWielowymiarowa[0][0];
        int najwieksza = tablicaWielowymiarowa[0][0];

        System.out.println("Piewsza Tablica");
        for (int poTablicach = 0; poTablicach < 2; poTablicach++) {
            if (poTablicach == 1) {
                System.out.println();
                System.out.println("Druga Tablica");
            }
            for (int wewnatrzTablic = 0; wewnatrzTablic < tablicaWielowymiarowa.length; wewnatrzTablic++) {
                System.out.println("Index[" + wewnatrzTablic + "] " + tablicaWielowymiarowa[poTablicach][wewnatrzTablic]);


                if (tablicaWielowymiarowa[poTablicach][wewnatrzTablic] > najwieksza) {
                    najwieksza = tablicaWielowymiarowa[poTablicach][wewnatrzTablic];


                } else if (tablicaWielowymiarowa[poTablicach][wewnatrzTablic] < najmniejsza) {
                    najmniejsza = tablicaWielowymiarowa[poTablicach][wewnatrzTablic];
                }

            }


        }

        System.out.println("-------------");
        for (int[] tab: tablicaWielowymiarowa) {
            for(int value: tab) {
                System.out.print(value + "  ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("Najwieksza liczba: " + najwieksza);
        System.out.println("Najmniejsza liczba: " + najmniejsza);
    }
}

