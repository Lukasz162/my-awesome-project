package p4_tablice.p4_1_tabliceWielowymiarowe;

public class TabliceWielowymiaroweIntro {
    public static void main(String[] args) {
        int[][] tablicaDwuwymiarowa = new int[2][2];
        //9    7
        //2    1
        int tablicaPierwsza[] = {9, 7};
        int drugaTablica[] = {2, 1};
        tablicaDwuwymiarowa[0] = tablicaPierwsza;
        tablicaDwuwymiarowa[1] = drugaTablica;

        int dlugosc = tablicaDwuwymiarowa.length;

// ForEach
        for (int x[]: tablicaDwuwymiarowa){
            for (int y :x){
                System.out.print(y + " ");

            }
            System.out.println();}


// for w for'ze
        System.out.println();
        for (int poTablicach = 0; poTablicach < dlugosc; poTablicach++) {
            for (int wewnatrzTablicy = 0; wewnatrzTablicy < dlugosc; wewnatrzTablicy++) {
                System.out.print(tablicaDwuwymiarowa[poTablicach][wewnatrzTablicy] + " ");

            }
            System.out.println();
        }
    }}


