package p4_tablice.p4_1_tabliceWielowymiarowe;

import java.util.Scanner;

public class TabliceWielowymiaroweZadanie1_4 {
    public static void main(String[] args) {
        int[][] tablicaDwuwymiarowa = new int[4][];
        int[] tablicaPierwsza = {1,2,7,54};
        int[] tablicaDruga = {2, 3,99,65,123,6432};
        int[] tablicaTrzecia = {4, 5, 6,12,53,76};
        int[] tablicaCzwarta = {7, 8, 9, 10,123,321,34};
        tablicaDwuwymiarowa[0] = tablicaPierwsza;
        tablicaDwuwymiarowa[1] = tablicaDruga;
        tablicaDwuwymiarowa[2] = tablicaTrzecia;
        tablicaDwuwymiarowa[3] = tablicaCzwarta;

        for (int poTablicach = 0; poTablicach < tablicaDwuwymiarowa.length ; poTablicach++) {
            for (int wewnatrzTablic = 0; wewnatrzTablic <= poTablicach ; wewnatrzTablic++) {
                System.out.print (tablicaDwuwymiarowa[poTablicach][wewnatrzTablic]+ "\t");
            }
            System.out.println();
        }
    }
}
