package p4_tablice;

public class Tablica2 {
    public static void main(String[] args) {
        int tablica1[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
// wyświetlanie wartości tablicy za pomocą for'a

        System.out.print("tablica:[");
        for (int index = 0; index < tablica1.length; index++) { // <--- zczytujemy długość tablicy
            if (index == tablica1.length - 1) {
                System.out.print(tablica1[index]);
            } else {
                System.out.print(tablica1[index] + ", ");        //<--- Index w []
            }
        }
        System.out.print("]");
    }
}
