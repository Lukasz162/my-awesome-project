package p11;

import java.util.ArrayList;
import java.util.List;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;

    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public static void main(String[] args) {
        List<Point> points = new ArrayList<>();
        points.add(new Point(3, 6));
        points.add(new Point(5, 2));
        points.add(new Point(1, -4));
        points.add(new Point(8, 7));

        points
                .stream()
//                .filter(point -> point.getX() > 4 && point.getY() > 4)

//                .mapToDouble(point -> (point.getX() + point.getY()) / 2 )
//                .sorted()
//                .peek(point -> {
//                    System.out.println(point.getX());
 .forEachOrdered(point -> {
//    });

                    System.out.print("X: " + point.getX() + " ");
                    System.out.println("Y: " + point.getY());
                });
    }
}

