package p11;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LambdaIntro {
    public static void main(String[] args) {

        List<String> names = new ArrayList<>();
        names.add("Olaf");
        names.add("Olala");
        names.add("Olga");
        names.add("Olgierd");
        names.add("Ala");
        names.add("Adam");


        List<String> collect = names
                .stream()
                .filter(name -> {
                    return name.contains("O");
                })
                .collect(Collectors.toList());

        collect
                .stream()
                .forEach(System.out::println);


        int[] numbers = {1, 5, 2, 6, 3, 2, 6, 7, 1, 6, 0};
int n = 1234;
boolean isPrime = IntStream.range(2,n-1).anyMatch(i ->{
    return n % i == 0;
});
        System.out.println(isPrime);

        IntStream.range(0, numbers.length - 1)
                .forEach(index -> {
                    if (index % 2 == 0) {
                        System.out.println(numbers[index]);
                    }
                });

        //        Arrays.stream(numbers)
//                .peek(System.out::println);


//                 .mapToInt(name -> {
//                .forEach(name -> System.out.println(name)); //  .forEach(System.out::println);
//                    return name.length();
//                })
//                .filter(length -> length > 4)

//                 .reduce((a, b) -> a + b)

//                .ifPresent(System.out::println);

//       if (optionalInt.isPresent()){
//           System.out.println(optionalInt.getAsInt());
//       }

//        SampleInterface sample = new SampleInterface() {
//            @Override
//            public double add(double d) {
//                return d + d;
//            }
//        };

//        doIt((b)-> b + b, 10);
//    }
//
//    public static void doIt(SampleInterface i, double b) {
//        System.out.println(i.add(b));

    }
}
