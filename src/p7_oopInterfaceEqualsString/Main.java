package p7_oopInterfaceEqualsString;

import p6_objects.Screen;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello!");

        Car car = new Car();
        Bike bike = new Bike();
        Car car1 = new Car();


        car.setManufactureYear(2000);
        car1.setManufactureYear(2005);
        bike.setManufactureYear(2018);

        car.setWheels(4);
        car1.setWheels(4);
        bike.setWheels(2);

        System.out.println(car.getManufactureYear());
        System.out.println(car1.getManufactureYear());
        System.out.println(bike.getManufactureYear());


//        bike.drive();
//        System.out.println(car.toString());
//        System.out.println(bike.toString());
//        System.out.println(car.equals(car1));
        do {
            System.out.println("Car price:");
            car.price = new Scanner(System.in).nextInt();
            System.out.println("Bike price:");
            bike.price = new Scanner(System.in).nextInt();

            System.out.println("What is better?");
            System.out.println("Car is from " + car.getManufactureYear() + " year.");
            System.out.println("Bike is from " + bike.getManufactureYear() + " year.");
            System.out.println("Which one is more expensive? ");

            int yesOrNot = new Scanner(System.in).nextInt();
            if (yesOrNot == 1) {
                priceCalculator(car.price, bike.price);
            }
        } while (true);

    }

    public static int priceCalculator(int firstVehicle, int secondVehicle) {
        int price;
        if (firstVehicle > secondVehicle) {
            price = firstVehicle - secondVehicle;
            System.out.println("Car is more expensive than bike: " + price + " zł");
        }else if(firstVehicle == secondVehicle) {
            System.out.println("Hmmm... I'm stupid, i don't know");
            return 0;
        } else {
            price = secondVehicle - firstVehicle;
            System.out.println("Bike is more expesive than car: " + price + " zł");
        }
        System.out.println("_________________________");
        return price;
    }
}
