package p7_oopInterfaceEqualsString;

public class Car extends Vehicle {

    @Override
    public String toString() {
        return "Car wheels: " + getWheels();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Car)) {
            return false;
        }
        Car car = (Car) obj;
        if (this == car) {
            return true;
        }

        return this.getWheels() == car.getWheels();

    }



}
