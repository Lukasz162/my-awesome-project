package p7_oopInterfaceEqualsString;

public class Vehicle {
   private int wheels;
    private int manufactureYear;
    int price;
    @Override
    public String toString(){
        return "Vehicle";
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public int getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(int manufactureYear) {
        this.manufactureYear = manufactureYear;
    }
}
