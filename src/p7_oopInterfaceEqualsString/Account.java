package p7_oopInterfaceEqualsString;

public abstract class Account {
    private double amount;


    public Account(double amount) {
        super();
        this.amount = amount;
    }


    public double getAmount() {
        return this.amount;
    }

    public abstract double getAmountAfterOneYear();
}