package p7_oopInterfaceEqualsString;

public class Enum {
    public enum TshitsSizes {
        XS(123), S(23), M(345), L(21), XL(12315);


        private int code;

        TshitsSizes(int code) {
            this.code = code;
        }

        public int getCode() {
            return this.code;
        }
    }
}


