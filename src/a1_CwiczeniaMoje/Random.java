package a1_CwiczeniaMoje;

import java.util.Scanner;

public class Random {

    private int[] board;

    public Random() {
        int[] board = new int[5];
    }

    public Random(int size) {
        this();
        setBoard(size);

    }

    public void setBoard(int size) {
        int[] board = new int[size];
        this.board = board;
    }

    public static void main(String[] args) {
        Random board = new Random();
        int x = new Scanner(System.in).nextInt();
        board.setBoard(x);

        System.out.println(board.board[x-1]);

    }
}