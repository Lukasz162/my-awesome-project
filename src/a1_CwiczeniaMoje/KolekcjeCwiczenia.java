package a1_CwiczeniaMoje;

import java.util.HashMap;
import java.util.Scanner;

public class KolekcjeCwiczenia {
    public static void main(String[] args) {

        HashMap<String, String> slownik = new HashMap<>();

        while (true) {

            System.out.println("Podaj słowo po Polsku");
            Scanner input = new Scanner(System.in);
            String load = input.nextLine();

            System.out.println("Podaj jego tłumaczenie po Angielsku");
            String englishWord = input.nextLine();

            System.out.println(load);
            slownik.put(load, englishWord);

            System.out.println("Wydrukować słownik?");
            input.nextLine();
            if (load.equalsIgnoreCase("Tak")) {

                System.out.println(slownik);
            }
        }
    }
}
