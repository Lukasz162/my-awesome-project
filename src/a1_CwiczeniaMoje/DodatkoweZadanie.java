package a1_CwiczeniaMoje;

import java.util.Scanner;

public class DodatkoweZadanie {
    public static void main(String[] args) {
        // Liczby pierwsze

        System.out.println("Hello World!\n" +
                "Please, write me a number.\n" +
                "Range: 0 - 2 000 000\n" +
                "I will show you some magic.\n");
        double numberFromUser;


        do {
            System.out.println("Your number: ");
            numberFromUser = new Scanner(System.in).nextDouble();
            if (numberFromUser > 2000000 || numberFromUser < 0) {                   // ten if określa nam  granice
                                                                                    // jeżeli będzie poza zasięgiem to "OUT OF RANGE" i wpisać trzeba ponownie
                System.out.println("OUT OF RANGE");
                continue;
            }
            liczbaPierwsza(numberFromUser);
        } while (numberFromUser != 0);

    }

    public static void liczbaPierwsza(double numberFromUser) {                  // Metoda statyczna określająca nam czy dana liczba jest liczba pierwsza czy nie
                                                                                // Użyty Double by nie wyjść poza granicami INT'a
        int count = 0;
        double liczbaPierwszaDouble = numberFromUser;
        System.out.println("Your number is: \n" + (int) liczbaPierwszaDouble);

        for (int index = 1; index <= numberFromUser; index++) {             // for do sprawdzania liczb
            if (numberFromUser % index == 0) {                              // podana liczba/index i reszta dzielenia = 0
                count++;                                                    //(rosnie co 1 i sprawdza wszystkie liczby do sprawdzonej)
                                                                            // jezeli reszta z dzielenia == 0 i wypadnie nam to wiecej niz 2 razy
                                                                            // to dana liczba nie jest liczba pierwsza
            }
        }
        if (numberFromUser == 1 || numberFromUser == 0) {                    // 0 i 1 nie sa liczbami pierwszymi, wyjątki w naszej metodzie
            System.out.print("Nie jest to liczba pierwsza\n");
            System.out.println("__________________");
        } else if (count == 2) {
            System.out.println("Liczba Pierwsza");
            System.out.println("__________________");
        } else if (count > 2) {
            System.out.println("Nie jest to liczba pierwsza");
            System.out.println("__________________");
        }
    }
}

