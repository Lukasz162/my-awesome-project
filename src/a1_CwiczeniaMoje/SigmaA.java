package a1_CwiczeniaMoje;

import java.util.Scanner;

public class SigmaA {
    public static void main(String[] args) {


        double z;            // z - Jest to zmienna, która wskazuje nam zakończenie podawania liczb.
        int y;              // y - Jest to zmienna, licząca nam ilość liczb
        double j;           // j - Jest to suma liczb
        int licznik = 0;    // licznik - będzie on nam liczyć pętle, które będziemy wykonywać
        z = 0;
        j = 0;
        y = 0;


        System.out.println("Podaj ilość liczb na których będziemy działać: ");
        y = new Scanner(System.in).nextInt();                                                // Wpisz "Y" - zakres działania, ilość liczb do wpisania

        System.out.println("Podaj "+y+" liczb a obliczymy średnią arytmetyczną");
        while (licznik < y) {                                                               // Dopóki licznik jest mniejszy od y wykonaj...
            z = new Scanner(System.in).nextInt();                                           // Podaj "Z"
            j = j + z;                                                                      // Dodawanie do siebie kolejnych liczb
            licznik++;                                                                      // Dodanie do licznika 1 (LICZNIK + 1)
        }


        double srednia = j / y;
        System.out.println("Srednia arytmetyczna: " + srednia);

        // Koniec obliczania średniej arytmetycznej... Obliczanie odchylenia

        licznik = 0;                                                                        // Wyzerowanie licznika
        int[] x = new int[y];                                                               // Deklaracja tablicy o liczbie miejsc wskazanej przez zmienną Y

        System.out.println("Powtórz je jeszcze raz");
        while (licznik < y) {

            x[licznik] = new Scanner(System.in).nextInt();                                  // Powtórne wpisywanie tych samych liczb
            licznik ++;                                                                     // Licznik +1
        }

        licznik = 0;                                                                        // wyzerowanie licznika
        double S = 0;
        while (licznik < y) {                                                               // Dopóki licznik < y, wykonaj...
            S += Math.pow(x[licznik] - srednia, 2);                                         // Obliczenie każdego elementu ze wzoru *(a1 - średnia)^2*
            licznik ++;
        }

        S = S / y;                                                                          //  Obczliczenie we wzorze Waroancji
        S = Math.sqrt(S);                                                                   // Pierwiastek z wariancji

        System.out.println("Odchylenie standardowe: " + S);
    }
}



