//public class DodatkoweZadanie {
//    public static void main(String[] args) {
//
//        System.out.println("Hello World!\n" +
//                "Please, write me a number.\n" +
//                "Range: 0 - 2 000 000\n" +
//                "I will show you some magic.\n");
//        double numberFromUser; //double?
//
//
//        do {
//            System.out.println("Your number: ");
//            numberFromUser = new Scanner(System.in).nextDouble();
//            if (numberFromUser > 2000000 || numberFromUser < 0) { //0 jest traktowany jak escape, poza tym czy jest sens rozpatrywać 0 i 1 jako liczbę pierwszą?
//                System.out.println("OUT OF RANGE");
//                continue; //sprytne, szkoda, że nie napisałeś kilka słów komentarza dlaczego ma tu być contiunue
//            }
//            liczbaPierwsza(numberFromUser);
//        } while (numberFromUser != 0);
//
//    }
//
//    public static void liczbaPierwsza(double numberFromUser) {         // nazwa metody nam nic nie mówi co ta metoda robi
//        // Użyty Double by nie wyjść poza granicami INT'a //a jest szansa, że możemy wyjść poza granice inta?
//        int count = 0; //lepiej counter
//        double liczbaPierwszaDouble = numberFromUser;
//        System.out.println("Your number is: \n" + (int) liczbaPierwszaDouble); //jeśli wpiszę cokolwiek co ma coś po przeciunku, a mogę to zrobić to okazuje się, że moja liczba jest liczbą pierwszą
//
//        for (int index = 1; index <= numberFromUser; index++) {             // for do sprawdzania liczb  -----------> czy jest sens sprawdzanie każdej liczby <= numberFromUser jako dzielnik?
//            if (numberFromUser % index == 0) {                              // podana liczba/index i reszta dzielenia = 0
//                count++;                                                    //(rosnie co 1 i sprawdza wszystkie liczby do sprawdzonej)
//                // jezeli reszta z dzielenia == 0 i wypadnie nam to wiecej niz 2 razy ------> skoro do stwierdzenia, że liczba nie jest pierwsza wystarczy counter = 3 to czy jest sens sprawdzanie dalej?
//                // to dana liczba nie jest liczba pierwsza
//            }
//        }
//        if (numberFromUser == 1 || numberFromUser == 0) {                    // 0 i 1 nie sa liczbami pierwszymi, wyjątki w naszej metodzie -----------> skoro tak to może odrzucać je zanim przekażemy do metody? albo na samym poczatku i dale metody nie wykonywać?
//            System.out.print("Nie jest to liczba pierwsza\n");
//            System.out.println("__________________");
//
//        } else if (count == 2) { // wiem, że to działa, ale dziwnie wygląda powinno być count == 2
//            System.out.println("Liczba Pierwsza");
//            System.out.println("__________________");
//        } else if (count > 2) {
//            System.out.println("Nie jest to liczba pierwsza");
//            System.out.println("__________________");
//        }
//    }
//}