package a1_CwiczeniaMoje;

import java.util.Scanner;

public class DodatkoweZadanieDwa {
    public static void main(String[] args) {

        int[] ourTableWithTenNumbers = new int[10];
        int tableLenght = ourTableWithTenNumbers.length;
        int ourRandomNumber = new Scanner(System.in).nextInt();
        int checkCounter = 0;

        for (int index = 0; index < tableLenght; index++) {                 //wypełnienie tablicy liczbami całkowitymi od 0 do 9.
            ourTableWithTenNumbers[index] = index;
        }
        for (int checkNumberA = 0; checkNumberA < tableLenght; checkNumberA++) {            //Przejscie po tablicy
            for (int checkNumberB = 0; checkNumberB < tableLenght; checkNumberB++) {


                if (ourTableWithTenNumbers[checkNumberA] + ourTableWithTenNumbers[checkNumberB] == ourRandomNumber) {        /*Sprawdzenie czy spełnione jest nasze założenie
                                                                                                                          A+B=Podana przez nas liczba*/
//                    System.out.print("A:" + ourTableWithTenNumbers[checkNumberA] + " ");
//                    System.out.println("B:" + ourTableWithTenNumbers[checkNumberB]);
//                    System.out.println("YES");
                    checkCounter++;
                }
            }
        }
        if (checkCounter == 0) {
            System.out.println("NO");
        } else {
            System.out.println("YES");
        }


    }
}

// Zasadę działania programu rozumiem. Nie jest on do końca dopracowany ale wiem o co chodzi i co z czym się je.
// For'ów tyle już pisaliśmy, że aż koszmary można po nich mieć. Ogólna zasada działania jest skończona.
// Jeżeli w tablicy znajdą się dwie liczby, których sumą będzie podana przez nas liczba to wyświetli "YES".
// Jeżeli nie to wyświetli "NO".
// Trochę brak czasu i zmęczenie dają mi się we znaki i dzisiaj muszę sobie zrobić taki wieczór przerwy bo inaczej wymęczy mnie to.