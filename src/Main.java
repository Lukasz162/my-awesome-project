public class Main {
    public static void main(String[] args) {
        Human adam;
        adam = new Human();

        Human ewa;
        ewa = new Human();

        adam.height = 185;
        adam.weight = 180;
        adam.sex = 'm';

        ewa.height = 150;
        ewa.weight = 50;
        ewa.sex = 'w';

        System.out.println("Adam's height is " + adam.height + " cm");
    }
}
